package ru.kuzin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.dto.IProjectDtoRepository;
import ru.kuzin.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<ProjectDTO> getEntityClass() {
        return ProjectDTO.class;
    }

}
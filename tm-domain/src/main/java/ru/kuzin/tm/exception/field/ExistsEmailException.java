package ru.kuzin.tm.exception.field;

public class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}